
// Import the http module using the required directive.
const { Console } = require('console');
const HTTP = require('http');
// Create a variable port and assign it with the value of 3000.
const PORT = 3001;

// Create a server using the createServer method that will listen in to the port provided above.
HTTP.createServer((req, res) => {

    // Console log in the terminal a message when the server is successfully running.
    console.log("testing");


    // Create a condition that when the login route is accessed, it will print a message to the user that they are in the login page.
    if(req.url == "/login"){

        res.writeHead(200, {"Content-Type": "text/plain"})
        res.write("You are currently in the login page")
        res.end()
    } else {
        res.writeHead(404, {"Content-Type": "text/plain"})
        res.write("Page not found")
        res.end()
    }



}).listen(PORT, () => console.log(`Server is connected to port ${PORT}`))